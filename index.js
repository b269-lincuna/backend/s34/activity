const express = require("express");

// app - server
const app = express();

const port = 3000;

// Middlewares - software that provides common services and capabilities to applications outside of what's offered by the operating system

// allows your app toread JSON data
app.use(express.json());

// allows you app to read data from any other forms
app.use(express.urlencoded({extended:true}));

//--------------------------------------

// [SECTION] ROUTES
// GET Method
app.get("/greet", (request,response) => {
	response.send("Hello from the /greet endpoint!");
});

//------------------
// POST Method
app.post("/hello", (request,response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

//------------------

let users = [];

app.post("/signup", (request,response) => {
	if ( request.body.userName!=="" && request.body.password!=="" ){
		users.push(request.body);
		response.send(`User ${request.body.userName} successfully registered!`);
	} else {
		response.send(`Please input both username and password!`);
	}

});

//Simple change password

app.patch("/change-password", (request,response) => {

	let message;

	for(let i=0; i < users.length; i++){
		if ( request.body.userName == users[i].userName ){
			users[i].password = request.body.password;

			message=`User ${request.body.userName}'s password has been updated!`;
			break;
		} else {
			message="User does not exist."
		}
	}
	response.send(message);
	
});


// app.listen(port,() => console.log(`Server running at ${port}`));


//****************************************************
//******************* ACTIVITY START *****************
//****************************************************

/* REFERENCES

npm init documentation  https://docs.npmjs.com/cli/v7/commands/npm-init

Express JS
https://expressjs.com/

What Is A Middleware
https://www.redhat.com/en/topics/middleware/what-is-middleware

Express JS json Method
http://expressjs.com/en/resources/middleware/body-parser.html#bodyparserjsonoptions

Express JS urlencoded Method
http://expressjs.com/en/resources/middleware/body-parser.html#bodyparserurlencodedoptions

*/
// console.log(users);

// [ 1 ]
app.get("/home", (request,response) => {
	response.send("Welcome to the home page");
});


// [ 3 ]
users.push({"userName":"paul1", "password":"user1"},
		   {"userName":"paul2", "password":"user2"},
	       {"userName":"paul3", "password":"user3"}
);

app.get("/items", (request,response) => {
	response.send(users);
});


// [ 5 ]
app.delete("/delete-item" , (request,response) => {

	let message;

	for(let i=0; i < users.length; i++){
		if ( request.body.userName == users[i].userName ){
			message=`User ${request.body.userName} has been deleted!`;
			users.splice(i,1);
			break;
		} else {
			message="User does not exist."
		}
	}

	// console.log(users);
	response.send(message);
});

//--------------------------------------

app.listen(port,() => console.log(`Server running at ${port}`));

//****************************************************
//*******************  ACTIVITY END  *****************
//****************************************************

/*
function deleteSpecificFriend(name){
    if(friendsList.length===0){
        alert("You currently have 0 friends. Add one first.");
    }else{ // have friends
        let isFriendExist =  friendsList.indexOf(name);
        //check if friend exist
        if (isFriendExist === -1) { // not exist in friendslist
            alert(name + " not exist in friendsList.");
        }else{ // exist and delete in friendslist
            friendsList.splice(isFriendExist , 1);
            console.log(friendsList);
        }
        
    }
}
*/